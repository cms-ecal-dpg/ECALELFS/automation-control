from setuptools import setup

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setup(name='ecalautoctrl',
      version='1.1.3',
      description='Python package to upload data from ECAL Automation workflows to the influxdb backend',
      long_description = long_description,
      url='https://gitlab.cern.ch/cms-ecal-dpg/ECALELFS/automation-control',
      author='Simone Pigazzini',
      author_email='simone.pigazzini@cern.ch',
      license='GPLv3',
      packages=[
          'ecalautoctrl'
      ],
      scripts=[
          'bin/ecalautomation.py',
          'bin/ecaldiskmon.py',
          'bin/ecalrunctrl.py',
          'bin/copysts.py',
          'bin/batchJobStatusChange.py',
          'bin/batchReprocess.py'
      ],
      classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: Linux",
      ],
      python_requires=">=3.6",
      install_requires=[
          'influxdb',
          'dbs3-client>=4.0.19',
          'python39-omsapi @ git+ssh://git@gitlab.cern.ch:7999/cmsoms/oms-api-client.git'
      ]
)
