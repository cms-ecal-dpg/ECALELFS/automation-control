#!/usr/bin/env python3

# script to mark all runs with a specific status for reprocessing.

import argparse
import subprocess
from sys import exit
from typing import Optional
from ecalautoctrl import RunCtrl

def cmdline_options():
    """
    Return the argparse instance to handle the cmdline options for this script.

    The function is needed by sphinx-argparse to easily generate the docs.
    """
    parser = argparse.ArgumentParser(description="""
    Mark all runs with a specific status for reprocessing.
    """, formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument('--db',
                        dest='dbname',
                        default='ecal_online_test',
                        type=str,
                        help='Database name, default is test db')

    parser.add_argument('-c', '--campaign',
                        dest='campaign',
                        default='prompt',
                        required=True,
                        type=str,
                        help='Processing campaign')

    parser.add_argument('--tasks',
                        dest='tasks',
                        default=None,
                        type=str,
                        nargs='+',
                        help='Which workflows to submit, space separated, default all')

    parser.add_argument('--status',
                        dest='status',
                        default='failed',
                        type=str,
                        nargs='+',
                        help="Task status(es) for which to update job status. Space separated. Default: 'failed'")

    parser.add_argument('--range',
                        dest='rrange',
                        default='',
                        type=lambda opt: opt.split(',') if opt else [None, None],
                        help='Define a range of runs to be reprocessed (min,max).')
    return parser

if __name__ == '__main__':
    opts = cmdline_options().parse_args()

    dbname = opts.dbname
    campaign = opts.campaign
    tasks = opts.tasks
    status = opts.status

    rrange = opts.rrange

    if len(rrange) != 2:
        print(f'--range requires exactly 2 values (comma separated). {len(rrange)} provided.')
        exit(-1)

    rctrl = RunCtrl(dbname=dbname, campaign=campaign)
 
    for task in tasks:
        for st in status:
            print(f"Mark runs in status '{st}' for reprocessing for task '{task}'.")
            runs = rctrl.getRuns({task: st})

            runslist = []
            for r in runs:
                runnumber = int(r['run_number'])
                appendrun = True
                if rrange != [None, None]:
                    rrange.sort()
                    if runnumber < int(rrange[0]) or runnumber > int(rrange[1]):
                        appendrun = False
                if appendrun:
                    runslist.append(r['run_number'])

            if not runslist:
                continue
            runslist = ','.join(runslist)

            cmd = f'ecalrunctrl.py --db {dbname} reprocess --campaign {campaign} --runs {runslist} --tasks {task}'
            print(cmd)
            ret = subprocess.run(cmd, shell=True, capture_output=True)
            if ret.returncode == 0:
                print(ret.stdout.decode().strip())
                print(ret.stderr.decode().strip())
                print(f"Marked runs for reprocessing.")
            else:
                print(ret.stderr.decode().strip())
                print(f"Failed marking runs for reprocessing.")

